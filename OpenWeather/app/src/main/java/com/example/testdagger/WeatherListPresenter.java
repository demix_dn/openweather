package com.example.testdagger;


import com.example.data.WeatherDataStore;
import com.example.data.entity.daily.DailyWeather;
import com.example.testdagger.di.PerActivity;
import com.example.testdagger.mapper.WeatherModelDataMapper;
import com.example.testdagger.model.WeatherModel;
import com.example.testdagger.utils.RxUtils;
import com.google.common.base.Preconditions;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@PerActivity
public class WeatherListPresenter {

    private CompositeSubscription presenterSubscription;
    private WeatherDataStore weatherDataStore;
    private WeatherModelDataMapper modelDataMapper;

    public void setup(WeatherDataStore weatherDataStore, WeatherModelDataMapper modelDataMapper){
        Preconditions.checkArgument(weatherDataStore != null, "Parameters cannot be null");
        this.presenterSubscription = new CompositeSubscription();
        this.weatherDataStore = weatherDataStore;
        this.modelDataMapper = modelDataMapper;
    }

    public void showLoadedData(){
        /* set loaded params */
        double longitude = 33.4141459; //Kremenchuk
        double latitude = 49.0684757; //Kremenchuk
        loadWeather(longitude, latitude);
    }

    public void loadWeather(double lon, double lat){
        Observer<List<WeatherModel>> observer = new Observer<List<WeatherModel>>() {
            @Override
            public void onCompleted() {
//                getViewState().showContent();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
//                getViewState().showError((Exception) e);
            }

            @Override
            public void onNext(List<WeatherModel> weatherModels) {
//                getViewState().setData(weatherModels);
            }
        };
        presenterSubscription.add(weatherDataStore.getWeekForecast(lon, lat)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(this::convertToModels)
                .subscribe(observer));
    }

    private Observable<List<WeatherModel>> convertToModels(List<DailyWeather> dailyWeathers) {
        return modelDataMapper.transformRx(dailyWeathers);
    }


    public void unsubscribe(){
        RxUtils.unsubscribe(presenterSubscription);
    }
}
