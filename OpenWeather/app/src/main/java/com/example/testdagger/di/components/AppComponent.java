package com.example.testdagger.di.components;

import android.content.Context;

import com.example.data.WeatherDataStore;
import com.example.testdagger.MainActivity;
import com.example.testdagger.di.modules.AppModule;
import com.example.testdagger.di.modules.WeatherApiModule;
import com.example.testdagger.mapper.WeatherModelDataMapper;
import com.example.testdagger.usecase.CurrentWeatherRepository;
import com.example.testdagger.utils.RxEventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, WeatherApiModule.class})
public interface AppComponent {
    Context context();

    void inject(MainActivity mainActivity);

    WeatherDataStore weatherDataStore();

    RxEventBus getEventBus();

    WeatherModelDataMapper dataMapper();

    CurrentWeatherRepository currentWeatherRepository();
}
