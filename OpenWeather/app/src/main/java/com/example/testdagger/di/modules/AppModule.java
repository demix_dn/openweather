package com.example.testdagger.di.modules;

import android.content.Context;

import com.example.data.WeatherConnection;
import com.example.data.WeatherDataStore;
import com.example.data.entity.CurrentWeatherMapper;
import com.example.data.entity.DailyWeatherMapper;
import com.example.data.net.WeatherAPI;
import com.example.data.net.WeatherConnectionImpl;
import com.example.data.repository.WeatherDataStoreImpl;
import com.example.database.datasource.DiskDataSource;
import com.example.database.datasource.DiskDataSourceImpl;
import com.example.testdagger.App;
import com.example.testdagger.usecase.CurrentWeatherRepository;
import com.example.testdagger.usecase.CurrentWeatherRepositoryDefaultImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    protected final App application;

    public AppModule(App application){
        this.application = application;
    }


    @Provides
    @Singleton
    Context provideContext(){
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    DiskDataSource provideDiskDataSource(Context context){
        return new DiskDataSourceImpl(context);
    }

    @Provides
    @Singleton
    WeatherDataStore provideWeatherDataStore(DiskDataSource diskDataSource,
                                             WeatherConnection weatherConnection,
                                             DailyWeatherMapper weatherMapper){
        return new WeatherDataStoreImpl(diskDataSource, weatherConnection, weatherMapper);
    }

    @Provides
    @Singleton
    WeatherConnection provideWeatherConnection(WeatherAPI api,
                                               DiskDataSource dataSource,
                                               DailyWeatherMapper dailyWeatherMapper,
                                               CurrentWeatherMapper currentWeatherMapper){
        return new WeatherConnectionImpl(api, dataSource, dailyWeatherMapper, currentWeatherMapper);
    }

    @Provides
    @Singleton
    CurrentWeatherRepository provideCurrentWeatherRepository(){
        return new CurrentWeatherRepositoryDefaultImpl();
    }
}
