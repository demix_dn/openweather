package com.example.testdagger.ui.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testdagger.R;
import com.example.testdagger.model.WeatherModel;
import com.google.common.base.Preconditions;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Date: 22.09.2016
 * Time: 19:36
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */

public class WeatherRecyclerAdapter extends RecyclerView.Adapter<WeatherRecyclerAdapter.WeatherViewHolder> {

    private static final int NIGHT_HOUR = 20;

    public interface OnItemClickListener{
        void onWeatherItemClicked(WeatherModel weatherModel);
    }

    private List<WeatherModel> mItems;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public WeatherRecyclerAdapter(Context context, List<WeatherModel> items){
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(mContext).inflate(R.layout.item_weather_day, parent, false);
        WeatherViewHolder viewHolder = new WeatherViewHolder(root);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        final WeatherModel item = mItems.get(position);

        //LogUtils.E(String.format(Locale.getDefault(), "Condition %s", item.getDescriptionRu()));

        boolean isNight = isNight(item.getDate() * 1000L);
        String iconName = isNight ? item.getIconNight() : item.getIconDay();
        int imageId = mContext.getResources().getIdentifier(iconName, "drawable", mContext.getPackageName());

        if(imageId==0)
            imageId = R.drawable.icon_50d;

        String titleStr = item.getDateString().substring(0,1).toUpperCase(Locale.getDefault())+item.getDateString().substring(1);

        holder.icon.setImageResource(imageId);
        holder.date.setText(titleStr);
        holder.tempLong.setText(mContext.getString(R.string.degree_celsius_long, item.getTempDayInt()));
        holder.pressure.setText(mContext.getString(R.string.pressure, item.getPressureInt()));
        holder.itemView.setOnClickListener(v -> onItemClick(item));
    }

    private boolean isNight(long timeInMillis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= NIGHT_HOUR;
    }

    private void onItemClick(WeatherModel item) {
        if(WeatherRecyclerAdapter.this.mOnItemClickListener!=null){
            WeatherRecyclerAdapter.this.mOnItemClickListener.onWeatherItemClicked(item);
        }
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void setWeathersCollection(Collection<WeatherModel> weathersCollection){
        this.validateWeatherCollection(weathersCollection);
        this.mItems = (List<WeatherModel>)weathersCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.mOnItemClickListener = onItemClickListener;
    }

    private void validateWeatherCollection(Collection<WeatherModel> weathersCollection){
        Preconditions.checkArgument(weathersCollection != null,"The list cannot be null");
    }

    static class WeatherViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivItemIcon)
        ImageView icon;
        @BindView(R.id.tvItemDay)
        TextView date;
        @BindView(R.id.tvItemTempLong) TextView tempLong;
        @BindView(R.id.tvItemPressure) TextView pressure;

        WeatherViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
