package com.example.testdagger.presentation.view.currentweather;

import com.arellomobile.mvp.MvpView;
import com.example.testdagger.model.CurrentWeatherModel;

public interface CurrentWeatherView extends MvpView {
    void showLoading();
    void hideLoading();
    void showError(Throwable ex);
    void showContent(CurrentWeatherModel weatherModel);
}
