package com.example.testdagger;

import com.arellomobile.mvp.MvpApplication;
import com.arellomobile.mvp.MvpFacade;
import com.example.data.net.ApiConst;
import com.example.testdagger.di.components.AppComponent;
import com.example.testdagger.di.components.DaggerAppComponent;
import com.example.testdagger.di.modules.AppModule;
import com.example.testdagger.di.modules.WeatherApiModule;
import com.squareup.leakcanary.LeakCanary;


public class App extends MvpApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate(){
        super.onCreate();
        initializeInjector();
        MvpFacade.init();
        if(BuildConfig.DEBUG){
            LeakCanary.install(this);
        }
    }

    private void initializeInjector() {
        this.appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .weatherApiModule(new WeatherApiModule(ApiConst.URLS.BASE_URL))
                .build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

}
