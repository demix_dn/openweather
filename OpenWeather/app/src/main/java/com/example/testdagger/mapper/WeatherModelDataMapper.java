package com.example.testdagger.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.data.entity.daily.DailyWeather;
import com.example.testdagger.model.WeatherModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Date: 07.09.2016
 * Time: 18:05
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Singleton
public class WeatherModelDataMapper {

    @Inject
    public WeatherModelDataMapper() {
    }

    @Nullable
    public WeatherModel transform(DailyWeather item){
        if(item == null) return null;
        return WeatherModel.builder()
                .date(item.getDate())
                .tempMin(item.getTempMin())
                .tempDay(item.getTempDay())
                .tempMax(item.getTempMax())
                .tempNight(item.getTempNight())
                .tempEve(item.getTempEve())
                .tempMorn(item.getTempMorn())
                .pressure(item.getPressure())
                .humidity(item.getHumidity())
                .titleEn(item.getTitleEn())
                .descriptionEn(item.getDescriptionEn())
                .titleRu(item.getTitleRu())
                .descriptionRu(item.getDescriptionRu())
                .iconDay(item.getIconDay())
                .iconNight(item.getIconNight())
                .windSpeed(item.getWindSpeed())
                .windDeg(item.getWindDeg())
                .clouds(item.getClouds())
                .cityName(item.getCityName())
                .build();
    }

    @NonNull
    public List<WeatherModel> transform(List<DailyWeather> items){
        List<WeatherModel> results = new ArrayList<>();
        if(items!=null && items.size()>0)
            for(DailyWeather item : items)
                results.add(transform(item));
        return results;
    }

    public Observable<List<WeatherModel>> transformRx(List<DailyWeather> items) {
        return Observable.just(transform(items));
    }
}
