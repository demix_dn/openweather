package com.example.testdagger.model;

import android.support.annotation.DrawableRes;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 22.09.2016
 * Time: 19:21
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */
@Data
@Builder
public class SubWeather {
    private String dayName;         //string value in format 'EEE.'
    private String description;     //string value
    @DrawableRes
    private int icon;
    private int tempMin;            //int value in celsius degrees
    private int tempMax;            //int value in celsius degrees
}
