package com.example.testdagger.di.modules;

import com.example.data.net.ApiModule;
import com.example.data.WeatherConnection;
import com.example.data.net.WeatherAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class WeatherApiModule {
    private final String apiEndpoint;

    public WeatherApiModule(String apiEndpoint) {
        this.apiEndpoint = apiEndpoint;
    }

    @Provides
    WeatherAPI provideWeatherAPI(){
        return new ApiModule(apiEndpoint).provideApi();
    }
}
