package com.example.testdagger.di.modules;

import android.app.Activity;

import com.example.testdagger.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Date: 07.09.2016
 * Time: 11:20
 * @author Aleks Sander
 * Project OpenWeather
 */
@Module
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity(){
        return this.activity;
    }
}
