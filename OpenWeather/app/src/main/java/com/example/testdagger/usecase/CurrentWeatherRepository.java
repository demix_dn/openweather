package com.example.testdagger.usecase;

import com.example.testdagger.model.CurrentWeatherModel;

import rx.Observable;

/**
 * Date: 22.09.2016
 * Time: 19:25
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */

public interface CurrentWeatherRepository {

    Observable<CurrentWeatherModel> getCurrentWeatherModel(long currentTimeInMillis, int cityId);

    Observable<CurrentWeatherModel> getCurrentWeatherModel(long currentTimeInMillis, double lon, double lat);
}
