package com.example.testdagger;

/**
 * Date: 01.09.2016
 * Time: 11:20
 * @author Aleks Sander
 * Project OpenWeather
 */
public class Constants {
    public static final String API_KEY = "jkf78ba=d78a#sad";
    public static final int SECONDS_IN_DAY = 24 * 60 * 60;
    public static final int SECONDS_IN_WEEK = 7 * SECONDS_IN_DAY;
}
