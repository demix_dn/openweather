package com.example.testdagger.di.components;

import android.app.Activity;

import com.example.testdagger.di.PerActivity;
import com.example.testdagger.di.modules.ActivityModule;

import dagger.Component;

/**
 * Date: 07.09.2016
 * Time: 11:20
 * @author Aleks Sander
 * Project OpenWeather
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity activity();
}
