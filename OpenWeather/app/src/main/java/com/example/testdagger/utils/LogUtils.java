package com.example.testdagger.utils;

import android.text.TextUtils;
import android.util.Log;

import com.example.testdagger.BuildConfig;

/**
 * Date: 07.09.2016
 * Time: 14:20
 *
 * @author Aleks Sander
 * TestDagger
 */
public class LogUtils {

    private static boolean globalShow = true;


    public static void setGlobalShow(boolean show) {
        globalShow = show;
    }

    public static void E(String msg){
        if(BuildConfig.DEBUG && globalShow && !TextUtils.isEmpty(msg)){
            Log.e(BuildConfig.APPLICATION_ID, msg);
        }
    }

    public static void E(Object obj){
        if(BuildConfig.DEBUG && globalShow && obj != null){
            Log.e(BuildConfig.APPLICATION_ID, obj.toString());
        }
    }

    public static void E(String tag, String msg){
        if(BuildConfig.DEBUG && globalShow && !TextUtils.isEmpty(msg)){
            Log.e(BuildConfig.APPLICATION_ID, msg);
        }
    }

    /**
     * Show log with ignore global show params.
     * @param msg Message for log
     * @param show Show trigger
     */
    public static void E(String msg, boolean show){
        if(BuildConfig.DEBUG && show && !TextUtils.isEmpty(msg)){
            Log.e(BuildConfig.APPLICATION_ID, msg);
        }
    }

    public static void E(Object obj, boolean show){
        if(BuildConfig.DEBUG && show && obj != null){
            Log.e(BuildConfig.APPLICATION_ID, obj.toString());
        }
    }

    public static void E(String tag, String msg, boolean show){
        if(BuildConfig.DEBUG && show && !TextUtils.isEmpty(msg)){
            Log.e(BuildConfig.APPLICATION_ID, msg);
        }
    }

}
