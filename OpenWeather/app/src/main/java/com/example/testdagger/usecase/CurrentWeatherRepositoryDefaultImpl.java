package com.example.testdagger.usecase;

import com.example.testdagger.model.CurrentWeatherModel;
import com.example.testdagger.model.SubWeather;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Date: 22.09.2016
 * Time: 19:30
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */
@Singleton
public class CurrentWeatherRepositoryDefaultImpl implements CurrentWeatherRepository {

    private static final SubWeather sub1 = SubWeather.builder().dayName("Пт.")
            .icon(0x7f02003f)
            .description("Немного облачно")
            .tempMin(7).tempMax(16)
            .build();
    private static final SubWeather sub2 = SubWeather.builder().dayName("Сб.")
            .icon(0x7f020041)
            .description("Рассеянные облака")
            .tempMin(8).tempMax(13)
            .build();
    private static final SubWeather sub3 = SubWeather.builder().dayName("Вс.")
            .icon(0x7f020044)
            .description("Легкий дождь")
            .tempMin(5).tempMax(12)
            .build();
    private static final SubWeather sub4 = SubWeather.builder().dayName("Пн.")
            .icon(0x7f02003f)
            .description("Немного облачно")
            .tempMin(6).tempMax(15)
            .build();

    private static final ArrayList<SubWeather> items = new ArrayList<>();

    static {
        items.add(sub1);
        items.add(sub2);
        items.add(sub3);
        items.add(sub4);
    }

    private static final CurrentWeatherModel current = CurrentWeatherModel.builder().date("Чт., 22 окт")
            .icon(0x7f020040)
            .description("Немного облачно")
            .currentTemp(8).tempMin(5).tempMax(13)
            .pressure(748).humidity(90)
            .subWeathers(items)
            .build();

    @Inject
    public CurrentWeatherRepositoryDefaultImpl() {
    }

    @Override
    public Observable<CurrentWeatherModel> getCurrentWeatherModel(long currentTimeInMillis, int cityId) {
        return Observable.just(current);
    }

    @Override
    public Observable<CurrentWeatherModel> getCurrentWeatherModel(long currentTimeInMillis, double lon, double lat) {
        return Observable.just(current);
    }
}
