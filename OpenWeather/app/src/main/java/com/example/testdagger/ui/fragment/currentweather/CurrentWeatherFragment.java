package com.example.testdagger.ui.fragment.currentweather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.data.entity.daily.DailyWeather;
import com.example.testdagger.MainActivity;
import com.example.testdagger.R;
import com.example.testdagger.model.CurrentWeatherModel;
import com.example.testdagger.model.SubWeather;
import com.example.testdagger.presentation.presenter.currentweather.CurrentWeatherPresenter;
import com.example.testdagger.presentation.view.currentweather.CurrentWeatherView;
import com.example.testdagger.ui.fragment.MvpFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CurrentWeatherFragment extends MvpFragment implements CurrentWeatherView {
    public static final String TAG = "CurrentWeatherFragment";

    @InjectPresenter
    CurrentWeatherPresenter mCurrentWeatherPresenter;

    private Unbinder unbinder;

    @BindView(R.id.ivCurrentWeatherIcon)
    ImageView mainIcon;
    @BindView(R.id.tvCurrentWeatherTemp)
    TextView mainTemp;
    @BindView(R.id.tvCurrentWeatherTempMinMax)
    TextView mainTempMinMax;
    @BindView(R.id.tvCurrentWeatherDescription)
    TextView mainDescription;
    @BindView(R.id.tvCurrentWeatherHumidity)
    TextView mainHumidity;
    @BindView(R.id.tvCurrentWeatherPressure)
    TextView mainPressure;

    @BindViews({R.id.llCurrentSubItem1, R.id.llCurrentSubItem2, R.id.llCurrentSubItem3, R.id.llCurrentSubItem4})
    List<LinearLayout> subItems;

    public static CurrentWeatherFragment newInstance() {
        CurrentWeatherFragment fragment = new CurrentWeatherFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_current_weather, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume(){
        super.onResume();
        if(getActivity() instanceof MainActivity){
            mCurrentWeatherPresenter.setup(((MainActivity) getActivity()).getComponent().currentWeatherRepository());
        }
        mCurrentWeatherPresenter.loadWeather(0);
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        if(unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void showLoading() {
        ((MainActivity) getActivity()).getComponent().weatherDataStore()
                .getWeekForecast(704147)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<DailyWeather>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<DailyWeather> dailyWeathers) {

                    }
                });
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(Throwable ex) {

    }

    @Override
    public void showContent(@NonNull CurrentWeatherModel model) {
        mainIcon.setImageResource(model.getIcon());
        mainTemp.setText(getString(R.string.degree_celsius, model.getCurrentTemp()));
        mainTempMinMax.setText(getString(R.string.temp_min_max, model.getTempMin(), model.getTempMax()));
        mainDescription.setText(model.getDescription());
        mainHumidity.setText(getString(R.string.humidity, model.getHumidity()));
        mainPressure.setText(getString(R.string.pressure, model.getPressure()));
        for(int i=0; i<model.getSubWeathers().size(); i++){
            View subView = subItems.get(i);
            SubWeather subItem = model.getSubWeathers().get(i);
            if(subItem != null && subView != null)
                showInSubitem(subView, subItem);
        }
    }

    private void showInSubitem(@NonNull View subView, @NonNull SubWeather subItem){
        ImageView icon = ButterKnife.findById(subView, R.id.ivWeatherSubItemIcon);
        TextView day = ButterKnife.findById(subView, R.id.tvWeatherSubItemDay);
        TextView desc = ButterKnife.findById(subView, R.id.tvWeatherSubItemDesc);
        TextView tempMinMax = ButterKnife.findById(subView, R.id.tvWeatherSubItemTemp);
        icon.setImageResource(subItem.getIcon());
        day.setText(subItem.getDayName());
        desc.setText(subItem.getDescription());
        tempMinMax.setText(getString(R.string.temp_min_max, subItem.getTempMin(), subItem.getTempMax()));
    }
}
