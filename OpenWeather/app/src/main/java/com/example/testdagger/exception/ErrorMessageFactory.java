package com.example.testdagger.exception;

import android.content.Context;

import com.example.testdagger.R;

/**
 * Date: 08.09.2016
 * Time: 12:58
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class ErrorMessageFactory {
    public ErrorMessageFactory() {
    }

    public static String create(Context context, Exception exception){
        String message = context.getString(R.string.exception_message_generic);
        //// TODO: 08.09.2016 generic messages for exception type
        return message;
    }
}
