package com.example.testdagger;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.testdagger.di.HasComponent;
import com.example.testdagger.di.components.AppComponent;
import com.example.testdagger.ui.fragment.currentweather.CurrentWeatherFragment;
import com.example.testdagger.utils.RxUtils;

import rx.subscriptions.CompositeSubscription;


public class MainActivity extends AppCompatActivity implements HasComponent<AppComponent> {

    private CompositeSubscription subscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getComponent().inject(this);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.mainContainer, CurrentWeatherFragment.newInstance())
                .commit();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        RxUtils.unsubscribe(subscription);
    }

    @Override
    public AppComponent getComponent() {
        return ((App)getApplication()).getAppComponent();
    }
}
