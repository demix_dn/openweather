package com.example.testdagger.model;

import android.support.annotation.DrawableRes;

import java.util.List;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 22.09.2016
 * Time: 19:15
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */
@Data
@Builder
public class CurrentWeatherModel {
    private String date;        //string value in format 'EEE., dd MMM'
    @DrawableRes
    private int icon;
    private int currentTemp;    //int in celsius degrees
    private int tempMin;        //int value in celsius degrees
    private int tempMax;        //int value in celsius degrees
    private String description; //localized (en, ru) string
    private int pressure;       //int value in mm
    private int humidity;       //int value in percent
    private List<SubWeather> subWeathers;

}
