package com.example.testdagger.presentation.presenter.currentweather;


import com.example.testdagger.model.CurrentWeatherModel;
import com.example.testdagger.presentation.view.currentweather.CurrentWeatherView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.testdagger.usecase.CurrentWeatherRepository;
import com.example.testdagger.utils.RxUtils;
import com.google.common.base.Preconditions;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

@InjectViewState
public class CurrentWeatherPresenter extends MvpPresenter<CurrentWeatherView> {

    private CurrentWeatherRepository repository;
    private CompositeSubscription subscription;

    public void setup(CurrentWeatherRepository repository){
        Preconditions.checkArgument(repository != null, "Argument cannot be null");
        this.repository = repository;
        subscription = new CompositeSubscription();
    }

    public void loadWeather(int cityId) {

        Observer<CurrentWeatherModel> observer = new Observer<CurrentWeatherModel>() {
            @Override
            public void onCompleted() {
                getViewState().hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                getViewState().showError(e);
                getViewState().hideLoading();
            }

            @Override
            public void onNext(CurrentWeatherModel weatherModel) {
                getViewState().showContent(weatherModel);
            }
        };
        getViewState().showLoading();
        subscription.add(repository.getCurrentWeatherModel(System.currentTimeMillis(), cityId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer));
    }

    public void onDestroy(){
        RxUtils.unsubscribe(subscription);
    }
}
