package com.example.testdagger.utils;

import rx.Subscription;

/**
 * Date: 06.09.2016
 * Time: 11:20
 * @author Aleks Sander
 * Project OpenWeather
 */
public class RxUtils {
    public static void unsubscribe(Subscription subscription){
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
