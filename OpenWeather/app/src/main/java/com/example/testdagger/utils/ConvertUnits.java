package com.example.testdagger.utils;

/**
 * Date: 21.07.2016 in Weather
 * Time: 11:20
 * @author Aleks Sander
 * Project OpenWeather
 */
public class ConvertUnits {
    private static final double PRESSURE_ASPECT = 1.3332239d;
    public static int hPa_to_mm(double hPaValue){
        return (int)(hPaValue/PRESSURE_ASPECT);
    }
}
