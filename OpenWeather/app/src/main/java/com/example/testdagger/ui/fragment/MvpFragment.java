package com.example.testdagger.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpDelegate;

/**
 * Date: 22.09.2016
 * Time: 17:35
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */

public class MvpFragment extends Fragment {
    private Bundle mTemporaryBundle = null;
    private MvpDelegate<? extends MvpFragment> mMvpDelegate;

    public MvpFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getMvpDelegate().onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(this.mTemporaryBundle != null) {
            this.getMvpDelegate().onCreate(this.mTemporaryBundle);
            this.mTemporaryBundle = null;
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void onStart() {
        super.onStart();
        this.getMvpDelegate().onStart();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.getMvpDelegate().onSaveInstanceState(outState);
    }

    public void onStop() {
        super.onStop();
        this.getMvpDelegate().onStop();
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.mTemporaryBundle = new Bundle();
        this.getMvpDelegate().onSaveInstanceState(this.mTemporaryBundle);
        this.getMvpDelegate().onDestroy();
    }

    public MvpDelegate getMvpDelegate() {
        if(this.mMvpDelegate == null) {
            this.mMvpDelegate = new MvpDelegate(this);
        }

        return this.mMvpDelegate;
    }
}
