package com.example.testdagger.model;

import com.example.testdagger.utils.ConvertUnits;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lombok.Builder;

/**
 * Date: 07.09.2016
 * Time: 17:57
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Builder
public class WeatherModel {

    private static final String DATE_FORMAT = "EEEE, dd MMMM, hh:mm";

    private int date; // in seconds
    private double tempDay;
    private double tempMin;
    private double tempMax;
    private double tempNight;
    private double tempEve;
    private double tempMorn;
    private double pressure;
    private int humidity;
    private String titleEn;
    private String descriptionEn;
    private String titleRu;
    private String descriptionRu;
    private String iconDay;
    private String iconNight;
    private double windSpeed;
    private double windDeg;
    private int clouds;
    private String cityName;

    public WeatherModel() {
    }

    public WeatherModel(int date, double tempDay, double tempMin, double tempMax, double tempNight,
                        double tempEve, double tempMorn, double pressure, int humidity,
                        String titleEn, String descriptionEn, String titleRu, String descriptionRu,
                        String iconDay, String iconNight, double windSpeed,
                        double windDeg, int clouds, String cityName) {
        this.date = date;
        this.tempDay = tempDay;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.tempNight = tempNight;
        this.tempEve = tempEve;
        this.tempMorn = tempMorn;
        this.pressure = pressure;
        this.humidity = humidity;
        this.titleEn = titleEn;
        this.descriptionEn = descriptionEn;
        this.titleRu = titleRu;
        this.descriptionRu = descriptionRu;
        this.iconDay = iconDay;
        this.iconNight = iconNight;
        this.windSpeed = windSpeed;
        this.windDeg = windDeg;
        this.clouds = clouds;
        this.cityName = cityName;
    }

    public String getDateString(){
        long millis = date * 1000L;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return simpleDateFormat.format(new Date(millis));
    }

    public int getTempMornInt(){
        return Double.valueOf(tempMorn).intValue();
    }

    public int getTempDayInt(){
        return Double.valueOf(tempDay).intValue();
    }

    public int getTempEveInt(){
        return Double.valueOf(tempEve).intValue();
    }

    public int getTempNightInt(){
        return Double.valueOf(tempNight).intValue();
    }

    public int getTempMinInt(){
        return Double.valueOf(tempMin).intValue();
    }

    public int getTempMaxInt(){
        return Double.valueOf(tempMax).intValue();
    }

    public int getPressureInt(){
        return ConvertUnits.hPa_to_mm(pressure);
    }


    public double getTempDay() {
        return tempDay;
    }

    public double getTempMin() {
        return tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public double getTempNight() {
        return tempNight;
    }

    public double getTempEve() {
        return tempEve;
    }

    public double getTempMorn() {
        return tempMorn;
    }

    public double getPressure() {
        return pressure;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setTempDay(double tempDay) {
        this.tempDay = tempDay;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public void setTempNight(double tempNight) {
        this.tempNight = tempNight;
    }

    public void setTempEve(double tempEve) {
        this.tempEve = tempEve;
    }

    public void setTempMorn(double tempMorn) {
        this.tempMorn = tempMorn;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getTitleRu() {
        return titleRu;
    }

    public void setTitleRu(String titleRu) {
        this.titleRu = titleRu;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getIconDay() {
        return iconDay;
    }

    public void setIconDay(String iconDay) {
        this.iconDay = iconDay;
    }

    public String getIconNight() {
        return iconNight;
    }

    public void setIconNight(String iconNight) {
        this.iconNight = iconNight;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getWindDeg() {
        return windDeg;
    }

    public void setWindDeg(double windDeg) {
        this.windDeg = windDeg;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherModel that = (WeatherModel) o;

        return date == that.date && Double.compare(that.tempDay, tempDay) == 0
                && (titleRu != null ? titleRu.equals(that.titleRu) : that.titleRu == null
                && (cityName != null ? cityName.equals(that.cityName) : that.cityName == null));

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = date;
        temp = Double.doubleToLongBits(tempDay);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (titleRu != null ? titleRu.hashCode() : 0);
        result = 31 * result + (cityName != null ? cityName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WeatherModel{" +
                "date=" + getDateString() +
                ", cityName='" + cityName + '\'' +
                ", tempDay=" + getTempDayInt() +
                ", pressure=" + getPressureInt() +
                ", humidity=" + humidity +
                ", windSpeed=" + windSpeed +
                '}';
    }
}
