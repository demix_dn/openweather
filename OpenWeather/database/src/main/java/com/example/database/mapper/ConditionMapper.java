package com.example.database.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.database.entity.output.Condition;
import com.example.database.entity.ConditionRO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Date: 09.09.2016
 * Time: 17:49
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class ConditionMapper {
    public ConditionMapper() {
        //empty
    }

    @NonNull
    private ConditionRO transform(JSONArray jsonArray){
        int id = jsonArray.optInt(0);
        String titleEn = jsonArray.optString(1);
        String descriptionEn = jsonArray.optString(2);
        String titleRu = jsonArray.optString(3);
        String descriptionRu = jsonArray.optString(4);
        String iconDay = jsonArray.optString(5);
        String iconNight = jsonArray.optString(6);
        return new ConditionRO(id, titleEn, descriptionEn, titleRu, descriptionRu, iconDay, iconNight);
    }

    public Observable<List<ConditionRO>> transformRx(@NonNull String jsonString){
        try {
            return Observable.just(transform(jsonString));
        } catch (JSONException e) {
            e.printStackTrace();
            return Observable.error(e);
        }
    }

    @NonNull
    private List<ConditionRO> transform(@NonNull String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray conditionsArray = jsonObject.optJSONArray("conditions");
        List<ConditionRO> cityList = new ArrayList<>();
        for(int i=0;i<conditionsArray.length();i++){
            ConditionRO item = transform(conditionsArray.optJSONArray(i));
            cityList.add(item);
        }
        return cityList;
    }

    @NonNull
    public List<Condition> transform(@NonNull List<ConditionRO> source){
        List<Condition> result = new ArrayList<>();
        for(ConditionRO item : source)
            result.add(transform(item));
        return result;
    }

    @Nullable
    public Condition transform(@NonNull ConditionRO s){
        return Condition.builder()
                .id(s.getId())
                .titleEn(s.getTitleEn())
                .descriptionEn(s.getDescriptionEn())
                .titleRu(s.getTitleRu())
                .descriptionRu(s.getDescriptionRu())
                .iconDay(s.getIconDay())
                .iconNight(s.getIconNight())
                .build();
    }
}
