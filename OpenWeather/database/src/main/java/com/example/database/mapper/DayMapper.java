package com.example.database.mapper;

import android.support.annotation.NonNull;

import com.example.database.entity.output.Day;
import com.example.database.entity.DayRO;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 09.09.2016
 * Time: 20:30
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class DayMapper {

    public DayMapper() {
    }

    @SuppressWarnings("unused")
    @NonNull
    public List<Day> transform(@NonNull List<DayRO> source){
        List<Day> result = new ArrayList<>();
        for(DayRO item : source)
            result.add(transform(item));
        return result;
    }

    @NonNull
    public Day transform(DayRO s){
        return Day.builder()
                .cityId(s.getCityId())
                .date(s.getDate())
                .tempDay(s.getTempDay())
                .tempMin(s.getTempMin())
                .tempMax(s.getTempMax())
                .tempNight(s.getTempNight())
                .tempEve(s.getTempEve())
                .tempMorn(s.getTempMorn())
                .pressure(s.getPressure())
                .humidity(s.getHumidity())
                .conditionId(s.getConditionId())
                .windSpeed(s.getWindSpeed())
                .windDeg(s.getWindDeg())
                .clouds(s.getClouds())
                .build();
    }
}
