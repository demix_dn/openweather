package com.example.database.entity.output;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 09.09.2016
 * Time: 19:56
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class Day {

    private int date;
    private double tempDay;
    private double tempMin;
    private double tempMax;
    private double tempNight;
    private double tempEve;
    private double tempMorn;
    private double pressure;
    private int humidity;
    private int conditionId;
    private double windSpeed;
    private double windDeg;
    private int clouds;
    private int cityId;

}
