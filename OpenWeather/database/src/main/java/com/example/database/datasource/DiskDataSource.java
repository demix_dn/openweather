package com.example.database.datasource;

import android.support.annotation.Nullable;

import com.example.database.entity.output.City;
import com.example.database.entity.output.Condition;
import com.example.database.entity.output.Day;
import com.example.database.entity.DayRO;

import java.util.List;

/**
 * Date: 09.09.2016
 * Time: 20:52
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@SuppressWarnings("unused")
public interface DiskDataSource {

    /**
     * Save cloud response in database storage
     *
     * @param itemList list of saved items
     * @return count of saved items
     */
    Integer saveWeather(List<DayRO> itemList);

    /**
     * Restore from database storage a list of {@link Day} with some condition
     *
     * @param startPeriod Start date in seconds. Default use current millis with convert in seconds.
     * @param endPeriod End date in seconds in past (less than start date).
     * @return a list of {@link Day}. If no objects match the condition, a list with zero objects
     * is returned.
     */
    List<Day> getHistoryDays(int startPeriod, int endPeriod);

    /**
     * Restore from database storage a list of {@link Day} with some condition
     *
     * @param startPeriod Start date in seconds. Default use current millis with convert in seconds.
     * @param endPeriod End date in seconds in future (greater than start date).
     * @return a list of {@link Day}. If no objects match the condition, a list with zero objects
     * is returned.
     */
    List<Day> getFutureDays(int startPeriod, int endPeriod);

    /**
     * Restore from database storage a list of {@link Day} with some condition
     *
     * @param offset Start of chain items
     * @param count Count of returned items
     * @param orderDesc Use the DESC sort. If false, used ASC sort.
     * @return a list of {@link Day}. If no objects match the condition, a list with zero objects
     * is returned.
     */
    List<Day> getDaysBy(int offset, int count, boolean orderDesc);

    /**
     * Get all object {@link com.example.database.entity.CityRO} from database
     *
     * @return a list of {@link com.example.database.entity.CityRO}. If no objects match the condition, a list with zero
     * objects is returned.
     */
    List<City> getCities();

    /**
     * Get object {@link com.example.database.entity.CityRO} from database by his id
     *
     * @param id id of the city
     * @return a {@link com.example.database.entity.CityRO}. If no objects match the condition, return null
     */
    @Nullable
    City getCityBy(int id);

    /**
     * Get object {@link com.example.database.entity.CityRO} from database by his name
     *
     * @param name name of the city
     * @return a {@link com.example.database.entity.CityRO}. If no objects match the condition, return null
     */
    @Nullable
    City getCityBy(String name);

    /**
     * Get all object {@link com.example.database.entity.ConditionRO} from database
     *
     * @return a list of {@link com.example.database.entity.ConditionRO}. If no objects match, a list with zero
     * objects is returned.
     */
    List<Condition> getConditions();

    /**
     * Get object {@link com.example.database.entity.ConditionRO} from database by his id
     *
     * @param id id of the condition
     * @return a {@link com.example.database.entity.ConditionRO}. If no objects match the condition, return null
     */
    @Nullable
    Condition getConditionBy(int id);
}
