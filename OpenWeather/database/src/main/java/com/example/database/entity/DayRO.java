package com.example.database.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Date: 09.09.2016
 * Time: 19:56
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class DayRO extends RealmObject {

    public static final String DATE = "date";

    @PrimaryKey
    private int date;
    private double tempDay;
    private double tempMin;
    private double tempMax;
    private double tempNight;
    private double tempEve;
    private double tempMorn;
    private double pressure;
    private int humidity;
    private int conditionId;
    private double windSpeed;
    private int windDeg;
    private int clouds;
    private int cityId;

    public DayRO() {
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public double getTempDay() {
        return tempDay;
    }

    public void setTempDay(double tempDay) {
        this.tempDay = tempDay;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public double getTempNight() {
        return tempNight;
    }

    public void setTempNight(double tempNight) {
        this.tempNight = tempNight;
    }

    public double getTempEve() {
        return tempEve;
    }

    public void setTempEve(double tempEve) {
        this.tempEve = tempEve;
    }

    public double getTempMorn() {
        return tempMorn;
    }

    public void setTempMorn(double tempMorn) {
        this.tempMorn = tempMorn;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getWindDeg() {
        return windDeg;
    }

    public void setWindDeg(int windDeg) {
        this.windDeg = windDeg;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "DayRO{" +
                "date=" + date +
                ", tempDay=" + tempDay +
                ", tempMin=" + tempMin +
                ", tempMax=" + tempMax +
                ", tempNight=" + tempNight +
                ", tempEve=" + tempEve +
                ", tempMorn=" + tempMorn +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", conditionId=" + conditionId +
                ", windSpeed=" + windSpeed +
                ", windDeg=" + windDeg +
                ", clouds=" + clouds +
                '}';
    }


}
