package com.example.database.entity.output;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 09.09.2016
 * Time: 16:38
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class Condition {

    private int id;
    private String titleEn;
    private String descriptionEn;
    private String titleRu;
    private String descriptionRu;
    private String iconDay;
    private String iconNight;
}
