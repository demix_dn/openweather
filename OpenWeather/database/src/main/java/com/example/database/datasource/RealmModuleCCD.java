package com.example.database.datasource;

import com.example.database.entity.CityRO;
import com.example.database.entity.ConditionRO;
import com.example.database.entity.DayRO;

import io.realm.annotations.RealmModule;

/**
 * Date: 12.09.2016
 * Time: 12:03
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@RealmModule(library = true, classes = {CityRO.class, ConditionRO.class, DayRO.class})
class RealmModuleCCD {
    //empty migration
}
