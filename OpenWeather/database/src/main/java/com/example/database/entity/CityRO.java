package com.example.database.entity;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Date: 09.09.2016
 * Time: 16:27
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@SuppressWarnings("unused")
public class CityRO extends RealmObject {

    public static final String CITY_ID = "id";
    public static final String CITY_NAME = "name";

    @PrimaryKey
    private int id;

    @Index
    @Required
    private String name;

    public CityRO() {
    }

    public CityRO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityRO cityRO = (CityRO) o;

        return id == cityRO.id && name.equals(cityRO.name);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CityRO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
