package com.example.database.entity;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Date: 09.09.2016
 * Time: 16:38
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@SuppressWarnings("unused")
public class ConditionRO extends RealmObject {

    public static final String ID = "mId";

    @PrimaryKey
    private int mId;

    @Required
    private String mTitleEn;

    @Required
    private String mDescriptionEn;

    @Required
    private String mTitleRu;

    @Required
    private String mDescriptionRu;

    @Required
    private String mIconDay;

    @Required
    private String mIconNight;

    public ConditionRO() {
    }

    public ConditionRO(int id, String titleEn, String descriptionEn, String titleRu, String descriptionRu, String iconDay, String iconNight) {
        this.mId = id;
        this.mTitleEn = titleEn;
        this.mDescriptionEn = descriptionEn;
        this.mTitleRu = titleRu;
        this.mDescriptionRu = descriptionRu;
        this.mIconDay = iconDay;
        this.mIconNight = iconNight;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitleEn() {
        return mTitleEn;
    }

    public void setTitleEn(String titleEn) {
        this.mTitleEn = titleEn;
    }

    public String getDescriptionEn() {
        return mDescriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.mDescriptionEn = descriptionEn;
    }

    public String getTitleRu() {
        return mTitleRu;
    }

    public void setTitleRu(String titleRu) {
        this.mTitleRu = titleRu;
    }

    public String getDescriptionRu() {
        return mDescriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.mDescriptionRu = descriptionRu;
    }

    public String getIconDay() {
        return mIconDay;
    }

    public void setIconDay(String iconDay) {
        this.mIconDay = iconDay;
    }

    public String getIconNight() {
        return mIconNight;
    }

    public void setIconNight(String iconNight) {
        this.mIconNight = iconNight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConditionRO that = (ConditionRO) o;

        return mId == that.mId
                && (mTitleEn != null ? mTitleEn.equals(that.mTitleEn) : that.mTitleEn == null
                && (mTitleRu != null ? !mTitleRu.equals(that.mTitleRu) : that.mTitleRu != null));

    }

    @Override
    public int hashCode() {
        int result = mId;
        result = 31 * result + (mTitleEn != null ? mTitleEn.hashCode() : 0);
        result = 31 * result + (mTitleRu != null ? mTitleRu.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ConditionRO{" +
                "mId=" + mId +
                ", mTitleEn='" + mTitleEn + '\'' +
                ", mDescriptionEn='" + mDescriptionEn + '\'' +
                ", mTitleRu='" + mTitleRu + '\'' +
                ", mDescriptionRu='" + mDescriptionRu + '\'' +
                ", mIconDay='" + mIconDay + '\'' +
                ", mIconNight='" + mIconNight + '\'' +
                '}';
    }
}
