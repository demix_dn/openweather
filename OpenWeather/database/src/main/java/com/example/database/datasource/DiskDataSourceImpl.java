package com.example.database.datasource;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.database.entity.CityRO;
import com.example.database.entity.ConditionRO;
import com.example.database.entity.DayRO;
import com.example.database.entity.output.City;
import com.example.database.entity.output.Condition;
import com.example.database.entity.output.Day;
import com.example.database.mapper.CitiesMapper;
import com.example.database.mapper.ConditionMapper;
import com.example.database.mapper.DayMapper;
import com.google.common.base.Preconditions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;

/**
 * Date: 09.09.2016
 * Time: 20:53
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class DiskDataSourceImpl implements DiskDataSource {


    private int count;

    public DiskDataSourceImpl(Context context) {
        initRealmConfig(context);
        initCities();
    }


    private final Observer<List<CityRO>> mOnCityCompleted = new Observer<List<CityRO>>() {
        @Override
        public void onCompleted() {
            if (mCitySaveSubscription!=null && !mCitySaveSubscription.isUnsubscribed())
                mCitySaveSubscription.unsubscribe();
            initConditions();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onNext(List<CityRO> cityROs) {
            saveCities(cityROs);
        }
    };
    private final Observer<List<ConditionRO>> mOnConditionCompleted = new Observer<List<ConditionRO>>(){

        @Override
        public void onCompleted() {
            if (mConditionSaveSubscription!=null && !mConditionSaveSubscription.isUnsubscribed())
                mConditionSaveSubscription.unsubscribe();
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onNext(List<ConditionRO> conditionROs) {
            saveConditions(conditionROs);
        }
    };

    private Subscription mCitySaveSubscription;
    private Subscription mConditionSaveSubscription;

    //<editor-fold desc="Private methods">
    private void initRealmConfig(Context context) {
        RealmConfiguration configuration = new RealmConfiguration.Builder(context)
                .modules(new RealmModuleCCD())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    private void initCities() {
        if(!checkCitiesExists()) {
            mCitySaveSubscription = loadCities().subscribe(mOnCityCompleted);
        }
    }

    private void initConditions() {
        if(!checkConditionsExists()){
            mConditionSaveSubscription = loadConditions().subscribe(mOnConditionCompleted);
        }
    }


    private Observable<List<CityRO>> loadCities(){
        String fileCities = "res/raw/cities.json";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileCities);
        CitiesMapper citiesMapper = new CitiesMapper();
        return jsonStringCities(inputStream).flatMap(citiesMapper::transformRx);
    }

    private Observable<List<ConditionRO>> loadConditions(){
        String fileConditions = "res/raw/conditions.json";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(fileConditions);
        ConditionMapper conditionMapper = new ConditionMapper();
        return jsonStringCities(inputStream).flatMap(conditionMapper::transformRx);
    }

    private boolean checkCitiesExists(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<CityRO> cityQuery = realm.where(CityRO.class);
        RealmResults<CityRO> cities = cityQuery.findAll();
        int count = cities.size();
        if(!realm.isClosed())
            realm.close();
        return count > 0;
    }

    private boolean checkConditionsExists(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<ConditionRO> conditionQuery = realm.where(ConditionRO.class);
        RealmResults<ConditionRO> conditions = conditionQuery.findAll();
        int count = conditions.size();
        if(!realm.isClosed())
            realm.close();
        return count > 0;
    }

    private void saveCities(List<CityRO> cities){
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> {
                for(CityRO city : cities)
                    realm1.copyToRealmOrUpdate(city);
            });
        }finally {
            if (realm != null){
                realm.close();
            }
        }
    }

    private void saveConditions(List<ConditionRO> conditions){
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> {
                for(ConditionRO item : conditions)
                    realm1.copyToRealmOrUpdate(item);
            });
        }finally {
            if (realm != null){
                realm.close();
            }
        }
    }

    private Observable<? extends String> jsonStringCities(InputStream inputStream){
        return bufferString(inputStream)
                .filter(s1 -> s1 != null)
                .map(s -> s + "\n")
                .toList()
                .flatMap(this::concatStringList);
    }

    @NonNull
    private Observable<? extends String> concatStringList(List<String> strings) {
        String result = "";
        for(String string : strings) result += string;
        return Observable.just(result);
    }

    private Observable<? extends String> bufferString(InputStream inputStream){
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine())!=null) {
                        subscriber.onNext(line);
                    }
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        });
    }
    //</editor-fold>


    @Override
    public Integer saveWeather(List<DayRO> source){
        count = 0;
        try {
            if(source!=null) {
                Realm realm = null;
                try{
                    realm = Realm.getDefaultInstance();
                    realm.executeTransaction(realm1 -> {
                        for (DayRO day : source){
                            if(day!=null) {
                                realm1.copyToRealmOrUpdate(day);
                                count++;
                            }
                        }
                    });
                }finally {
                    if (realm != null){
                        realm.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @NonNull
    @Override
    public List<Day> getHistoryDays(int startPeriod, int endPeriod) {
        //check args
        Preconditions.checkArgument(startPeriod > 0, "Start date must be greater than 0");
        Preconditions.checkArgument(endPeriod > 0, "End date must be greater than 0");
        Preconditions.checkArgument(startPeriod > endPeriod, "Start date must be greater than end date");
        //restore items
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<DayRO> daysQuery = realm.where(DayRO.class);
        RealmResults<DayRO> daysResult = daysQuery
                .greaterThanOrEqualTo(DayRO.DATE, endPeriod)
                .lessThanOrEqualTo(DayRO.DATE, startPeriod)
                .findAll()
                .sort(DayRO.DATE, Sort.DESCENDING);
        List<Day> days = new ArrayList<>();
        DayMapper mapper = new DayMapper();

        for(DayRO item : daysResult)
            days.add(mapper.transform(item));

        if(!realm.isClosed())
            realm.close();
        return days;
    }

    @NonNull
    @Override
    public List<Day> getFutureDays(int startPeriod, int endPeriod) {
        //check args
        Preconditions.checkArgument(startPeriod > 0, "Start date must be greater than 0");
        Preconditions.checkArgument(endPeriod > 0, "End date must be greater than 0");
        Preconditions.checkArgument(startPeriod < endPeriod, "Start date must be less than end date");
        //restore items
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<DayRO> daysQuery = realm.where(DayRO.class);
        RealmResults<DayRO> daysResult = daysQuery
                .greaterThanOrEqualTo(DayRO.DATE, startPeriod)
                .lessThanOrEqualTo(DayRO.DATE, endPeriod)
                .findAll()
                .sort(DayRO.DATE, Sort.DESCENDING);
        List<Day> days = new ArrayList<>();
        DayMapper mapper = new DayMapper();

        for(DayRO item : daysResult)
            days.add(mapper.transform(item));

        if(!realm.isClosed())
            realm.close();
        return days;
    }

    @Override
    public List<Day> getDaysBy(int offset, int count, boolean orderDesc) {
        //check args
        Preconditions.checkArgument(offset >= 0, "Offset cannot be less than 0");
        Preconditions.checkArgument(count >= 0, "Count cannot be less than 0");
        //restore items
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<DayRO> daysQuery = realm.where(DayRO.class);
        RealmResults<DayRO> daysResult = daysQuery
                .findAll()
                .sort(DayRO.DATE, orderDesc ? Sort.DESCENDING : Sort.ASCENDING);
        List<Day> days = new ArrayList<>();

        int resultSize = daysResult.size();
        if(resultSize < offset)
            return days;
        if(resultSize < (offset + count))
            count = resultSize - offset;

        DayMapper mapper = new DayMapper();

        for(DayRO item : daysResult.subList(offset, offset + count))
            days.add(mapper.transform(item));

        if(!realm.isClosed())
            realm.close();
        return days;
    }

    @NonNull
    @Override
    public List<City> getCities() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<CityRO> cityQuery = realm.where(CityRO.class);
        RealmResults<CityRO> cities = cityQuery.findAll();
        List<CityRO> results = new ArrayList<>();
        for(CityRO city : cities)
            results.add(city);
        CitiesMapper mapper = new CitiesMapper();
        List<City> resultCities = mapper.transform(results);
        if(!realm.isClosed())
            realm.close();
        return resultCities;
    }

    @Nullable
    @Override
    public City getCityBy(int id) {
        Preconditions.checkArgument(id > 0, "Id must be greater than 0");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<CityRO> cityQuery = realm.where(CityRO.class);
        CityRO result = cityQuery.equalTo(CityRO.CITY_ID, id).findFirst();
        CitiesMapper mapper = new CitiesMapper();
        City resultCity = mapper.transform(result);
        if(!realm.isClosed())
            realm.close();
        return resultCity;
    }

    @Nullable
    @Override
    public City getCityBy(String name) {
        Preconditions.checkNotNull(name, "Name cannot be NULL");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<CityRO> cityQuery = realm.where(CityRO.class);
        CityRO result = cityQuery.equalTo(CityRO.CITY_NAME, name).findFirst();
        CitiesMapper mapper = new CitiesMapper();
        City resultCity = mapper.transform(result);
        if(!realm.isClosed())
            realm.close();
        return resultCity;
    }

    @NonNull
    @Override
    public List<Condition> getConditions(){
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<ConditionRO> conditionQuery = realm.where(ConditionRO.class);
        RealmResults<ConditionRO> conditions = conditionQuery.findAll();
        List<ConditionRO> results = new ArrayList<>();
        for(ConditionRO item : conditions)
            results.add(item);
        ConditionMapper mapper = new ConditionMapper();
        List<Condition> resultConditions = mapper.transform(results);
        if(!realm.isClosed())
            realm.close();
        return resultConditions;
    }

    @Nullable
    @Override
    public Condition getConditionBy(int id) {
        Preconditions.checkArgument(id > 0, "Id must be greater than 0");

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<ConditionRO> conditionQuery = realm.where(ConditionRO.class);
        ConditionRO result = conditionQuery.equalTo(ConditionRO.ID, id).findFirst();
        ConditionMapper mapper = new ConditionMapper();
        Condition resultCondition = mapper.transform(result);
        if(!realm.isClosed())
            realm.close();
        return resultCondition;
    }
}
