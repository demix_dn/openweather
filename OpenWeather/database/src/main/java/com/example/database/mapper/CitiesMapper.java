package com.example.database.mapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.database.entity.output.City;
import com.example.database.entity.CityRO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Date: 09.09.2016
 * Time: 16:59
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
public class CitiesMapper {
    public CitiesMapper() {
        //empty
    }

    @NonNull
    private List<CityRO> transform(@NonNull String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray citiesArray = jsonObject.optJSONArray("cities");
        List<CityRO> cityList = new ArrayList<>();
        for(int i=0;i<citiesArray.length();i++){
            CityRO city = transform(citiesArray.optJSONArray(i));
            cityList.add(city);
        }
        return cityList;
    }

    public Observable<List<CityRO>> transformRx(@NonNull String jsonString){
        try {
            return Observable.just(transform(jsonString));
        } catch (JSONException e) {
            e.printStackTrace();
            return Observable.error(e);
        }
    }

    @NonNull
    private CityRO transform(JSONArray jsonArray) {
        int cityId = jsonArray.optInt(0);
        String cityName = jsonArray.optString(1);
        return new CityRO(cityId, cityName);
    }

    @NonNull
    public List<City> transform(@NonNull List<CityRO> source){
        List<City> result = new ArrayList<>();
        for(CityRO item : source)
            result.add(transform(item));
        return result;
    }

    @Nullable
    public City transform(@NonNull CityRO source){
        return City.builder().id(source.getId()).name(source.getName()).build();
    }
}
