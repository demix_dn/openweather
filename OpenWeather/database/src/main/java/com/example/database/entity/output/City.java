package com.example.database.entity.output;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 09.09.2016
 * Time: 16:27
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class City{

    private int id;
    private String name;


}
