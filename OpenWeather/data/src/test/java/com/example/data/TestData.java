package com.example.data;

/**
 * Date: 02.09.2016
 * Time: 11:10
 * @author Aleks Sander
 * Project OpenWeather
 */
public class TestData {
    public static final String FAKE_FORECAST_1 = "{\n" +
            "  \"city\": {\n" +
            "    \"id\": 713101,\n" +
            "    \"name\": \"Avtozavodskiy Rayon\",\n" +
            "    \"coord\": {\n" +
            "      \"lon\": 33.416672,\n" +
            "      \"lat\": 49.066669\n" +
            "    },\n" +
            "    \"country\": \"UA\",\n" +
            "    \"population\": 0,\n" +
            "    \"sys\": {\n" +
            "      \"population\": 0\n" +
            "    }\n" +
            "  },\n" +
            "  \"cod\": \"200\",\n" +
            "  \"message\": 0.0351,\n" +
            "  \"cnt\": 40,\n" +
            "  \"list\": [\n" +
            "    {\n" +
            "      \"dt\": 1472839200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 15.95,\n" +
            "        \"temp_min\": 15.95,\n" +
            "        \"temp_max\": 15.95,\n" +
            "        \"pressure\": 1018.56,\n" +
            "        \"sea_level\": 1030.08,\n" +
            "        \"grnd_level\": 1018.56,\n" +
            "        \"humidity\": 76,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 1.84,\n" +
            "        \"deg\": 235.004\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-02 18:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472850000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 12.78,\n" +
            "        \"temp_min\": 12.78,\n" +
            "        \"temp_max\": 12.78,\n" +
            "        \"pressure\": 1018.39,\n" +
            "        \"sea_level\": 1030.01,\n" +
            "        \"grnd_level\": 1018.39,\n" +
            "        \"humidity\": 85,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 1.22,\n" +
            "        \"deg\": 223.003\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-02 21:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472860800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 11.28,\n" +
            "        \"temp_min\": 11.28,\n" +
            "        \"temp_max\": 11.28,\n" +
            "        \"pressure\": 1018.21,\n" +
            "        \"sea_level\": 1029.85,\n" +
            "        \"grnd_level\": 1018.21,\n" +
            "        \"humidity\": 86,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 1.12,\n" +
            "        \"deg\": 217.501\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 00:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472871600,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 10.56,\n" +
            "        \"temp_min\": 10.56,\n" +
            "        \"temp_max\": 10.56,\n" +
            "        \"pressure\": 1018.03,\n" +
            "        \"sea_level\": 1029.68,\n" +
            "        \"grnd_level\": 1018.03,\n" +
            "        \"humidity\": 85,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02n\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 24\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 1.22,\n" +
            "        \"deg\": 171\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"n\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 03:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472882400,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 19.36,\n" +
            "        \"temp_min\": 19.36,\n" +
            "        \"temp_max\": 19.36,\n" +
            "        \"pressure\": 1018.24,\n" +
            "        \"sea_level\": 1029.57,\n" +
            "        \"grnd_level\": 1018.24,\n" +
            "        \"humidity\": 68,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 803,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"broken clouds\",\n" +
            "          \"icon\": \"04d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 56\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 1.98,\n" +
            "        \"deg\": 184.005\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 06:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472893200,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 24.13,\n" +
            "        \"temp_min\": 24.13,\n" +
            "        \"temp_max\": 24.13,\n" +
            "        \"pressure\": 1017.93,\n" +
            "        \"sea_level\": 1029.2,\n" +
            "        \"grnd_level\": 1017.93,\n" +
            "        \"humidity\": 62,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 801,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"few clouds\",\n" +
            "          \"icon\": \"02d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 20\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2.92,\n" +
            "        \"deg\": 207.001\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 09:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472904000,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 25.46,\n" +
            "        \"temp_min\": 25.46,\n" +
            "        \"temp_max\": 25.46,\n" +
            "        \"pressure\": 1017.04,\n" +
            "        \"sea_level\": 1028.25,\n" +
            "        \"grnd_level\": 1017.04,\n" +
            "        \"humidity\": 55,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 802,\n" +
            "          \"main\": \"Clouds\",\n" +
            "          \"description\": \"scattered clouds\",\n" +
            "          \"icon\": \"03d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 36\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2.86,\n" +
            "        \"deg\": 253.004\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 12:00:00\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"dt\": 1472914800,\n" +
            "      \"main\": {\n" +
            "        \"temp\": 24.61,\n" +
            "        \"temp_min\": 24.61,\n" +
            "        \"temp_max\": 24.61,\n" +
            "        \"pressure\": 1016.46,\n" +
            "        \"sea_level\": 1027.68,\n" +
            "        \"grnd_level\": 1016.46,\n" +
            "        \"humidity\": 55,\n" +
            "        \"temp_kf\": 0\n" +
            "      },\n" +
            "      \"weather\": [\n" +
            "        {\n" +
            "          \"id\": 800,\n" +
            "          \"main\": \"Clear\",\n" +
            "          \"description\": \"clear sky\",\n" +
            "          \"icon\": \"01d\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "      },\n" +
            "      \"wind\": {\n" +
            "        \"speed\": 2,\n" +
            "        \"deg\": 287.5\n" +
            "      },\n" +
            "      \"sys\": {\n" +
            "        \"pod\": \"d\"\n" +
            "      },\n" +
            "      \"dt_txt\": \"2016-09-03 15:00:00\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
