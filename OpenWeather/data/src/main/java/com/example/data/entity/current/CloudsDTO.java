package com.example.data.entity.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 14.09.2016
 * Time: 16:06
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class CloudsDTO {
    @SerializedName("all")
    @Expose
    private int all;

    public static CloudsDTO getDefault(){
        return CloudsDTO.builder().all(0).build();
    }
}
