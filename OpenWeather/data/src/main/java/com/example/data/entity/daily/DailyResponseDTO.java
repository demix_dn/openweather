package com.example.data.entity.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

/**
 * Date: 08.09.2016
 * Time: 15:47
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
public class DailyResponseDTO {
    @SerializedName("city")
    @Expose
    private CityDTO city;
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private double message;
    @SerializedName("cnt")
    @Expose
    private int cnt;
    @SerializedName("list")
    @Expose
    private List<DailyItemDTO> dailyItems;
}
