package com.example.data.net;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Date: 22.07.2016 in Weather
 * Time: 11:09
 * @author Aleks Sander
 * Project OpenWeather
 */
public class ApiConst {

    public static final class URLS {
        public static final String BASE_URL = "http://api.openweathermap.org";
    }
    static final class PARAMS {
        static final String APPID = "1f0402f014d6e14ae3fe88f3b8971a82";
        static final String UNIT_METRICS = "metric";
        static final HttpLoggingInterceptor.Level LOG_LEVEL = HttpLoggingInterceptor.Level.BODY;
        static final String DAILY_FORECAST_COUNT = String.valueOf(7);
        static final int DEFAULT_OFFSET = 0;
        static final int DEFAULT_TIMEOUT = 5;
    }
}
