package com.example.data.entity.current;

import android.support.annotation.Nullable;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 15.09.2016
 * Time: 12:15
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */
@Data
@Builder
public class CurrentWeather {
    private int date;
    private int cityId;
    @Nullable private String cityName;
    private int conditionId;
    private double temp;
    private double tempMin;
    private double tempMax;
    private double pressure;
    private double seaLevel;
    private double groundLevel;
    private int humidity;
    private double windSpeed;
    private double windDegrees;
    private int clouds;
    @Nullable private String country;
    private int sunrise;
    private int sunset;
}
