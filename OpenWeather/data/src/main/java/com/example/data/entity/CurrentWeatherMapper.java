package com.example.data.entity;

import android.support.annotation.NonNull;

import com.example.data.entity.current.CloudsDTO;
import com.example.data.entity.current.CurrentResponseDTO;
import com.example.data.entity.current.CurrentWeather;
import com.example.data.entity.current.SysDTO;
import com.example.data.entity.current.WindDTO;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Date: 14.09.2016
 * Time: 16:15
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Singleton
public class CurrentWeatherMapper {

    @Inject
    public CurrentWeatherMapper() {
        //empty
    }

    @NonNull
    public CurrentWeather transform(@NonNull CurrentResponseDTO response) throws NullPointerException {

        //if weather data or main data is null - generate NPE
        if(response.getCondition() == null && response.getMain() == null)
            throw new NullPointerException("Some important data is null obj");

        //if clouds obj is null then set default obj
        if(response.getClouds() == null)
            response.setClouds(CloudsDTO.getDefault());

        //if wind obj is null then set default obj
        if(response.getWind() == null)
            response.setWind(WindDTO.getDefault());

        //if sys obj is null then set default obj
        if(response.getSys() == null)
            response.setSys(SysDTO.getDefault());


        return CurrentWeather.builder()
                .date(response.getDt())
                .cityId(response.getId())
                .cityName(response.getName())
                .conditionId(response.getCondition().getId())
                .temp(response.getMain().getTemp())
                .tempMin(response.getMain().getTempMin())
                .tempMax(response.getMain().getTempMax())
                .pressure(response.getMain().getPressure())
                .seaLevel(response.getMain().getSeaLevel())
                .groundLevel(response.getMain().getGrndLevel())
                .humidity(response.getMain().getHumidity())
                .windSpeed(response.getWind().getSpeed())
                .windDegrees(response.getWind().getDeg())
                .clouds(response.getClouds().getAll())
                .country(response.getSys().getCountry())
                .sunrise(response.getSys().getSunrise())
                .sunrise(response.getSys().getSunset())
                .build();
    }

    public Observable<? extends CurrentWeather> transformRx(CurrentResponseDTO responseDTO) {
        return Observable.just(transform(responseDTO));
    }
}
