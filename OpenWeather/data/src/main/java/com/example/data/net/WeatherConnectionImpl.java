package com.example.data.net;

import android.support.annotation.NonNull;

import com.example.data.WeatherConnection;
import com.example.data.entity.CurrentWeatherMapper;
import com.example.data.entity.DailyWeatherMapper;
import com.example.data.entity.current.CurrentWeather;
import com.example.database.datasource.DiskDataSource;
import com.example.database.entity.output.Day;
import com.example.database.entity.DayRO;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * WeatherConnection implementations
 */
public class WeatherConnectionImpl implements WeatherConnection {

    private final WeatherAPI weatherAPI;
    private final DiskDataSource diskDataSource;
    private final DailyWeatherMapper dailyWeatherMapper;
    private final CurrentWeatherMapper currentWeatherMapper;

    @Inject
    public WeatherConnectionImpl(WeatherAPI weatherAPI, DiskDataSource dataStore,
                                 DailyWeatherMapper mapper, CurrentWeatherMapper currentWeatherMapper){
        if(dataStore==null || weatherAPI == null || mapper == null || currentWeatherMapper == null)
            throw new IllegalArgumentException("The constructor parameters cannot be null!");
        this.weatherAPI = weatherAPI;
        this.diskDataSource = dataStore;
        this.dailyWeatherMapper = mapper;
        this.currentWeatherMapper = currentWeatherMapper;
    }

    @Override
    public Observable<List<Day>> forecastByCoords(String lat, String lon) {
        return weatherAPI.dailyForecastBy(lat, lon,
                ApiConst.PARAMS.DAILY_FORECAST_COUNT,
                ApiConst.PARAMS.APPID,
                ApiConst.PARAMS.UNIT_METRICS)
                .flatMap(dailyWeatherMapper::transformRx)
                .flatMap(this::saveWeather)
                .flatMap(this::getLastItems);
    }

    @Override
    public Observable<List<Day>> forecastByCityId(String cityId) {
        return weatherAPI.dailyForecastBy(cityId, ApiConst.PARAMS.DAILY_FORECAST_COUNT,
                ApiConst.PARAMS.APPID, ApiConst.PARAMS.UNIT_METRICS)
                .flatMap(dailyWeatherMapper::transformRx)
                .flatMap(this::saveWeather)
                .flatMap(this::getLastItems);
    }

    @Override
    public Observable<CurrentWeather> currentWeather(String lat, String lon) {
        return weatherAPI.currentWeather(lat, lon, ApiConst.PARAMS.APPID, ApiConst.PARAMS.UNIT_METRICS)
                .flatMap(currentWeatherMapper::transformRx);
    }

    @Override
    public Observable<CurrentWeather> currentWeather(String cityId) {
        return weatherAPI.currentWeather(cityId, ApiConst.PARAMS.APPID, ApiConst.PARAMS.UNIT_METRICS)
                .flatMap(currentWeatherMapper::transformRx);
    }

    private Observable<Integer> saveWeather(@NonNull List<DayRO> items){
        return Observable.just(diskDataSource.saveWeather(items));
    }

    private Observable<List<Day>> getLastItems(Integer count){
        return Observable.just(diskDataSource.getDaysBy(ApiConst.PARAMS.DEFAULT_OFFSET, count, true));
    }
}
