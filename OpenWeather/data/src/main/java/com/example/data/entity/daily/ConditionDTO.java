package com.example.data.entity.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Date: 09.09.2016
 * Time: 20:21
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
public class ConditionDTO {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("main")
    @Expose
    private String main;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("icon")
    @Expose
    private String icon;

}
