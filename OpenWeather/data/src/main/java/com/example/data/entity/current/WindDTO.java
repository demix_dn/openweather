package com.example.data.entity.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 14.09.2016
 * Time: 16:07
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class WindDTO {
    @SerializedName("speed")
    @Expose
    private double speed;
    @SerializedName("deg")
    @Expose
    private double deg;

    public static WindDTO getDefault() {
        return WindDTO.builder().deg(0.0).speed(0.0).build();
    }
}
