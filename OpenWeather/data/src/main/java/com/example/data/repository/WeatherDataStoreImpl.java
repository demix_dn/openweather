package com.example.data.repository;

import com.example.data.WeatherConnection;
import com.example.data.WeatherDataStore;
import com.example.data.entity.DailyWeatherMapper;
import com.example.data.entity.current.CurrentWeather;
import com.example.data.entity.daily.DailyWeather;
import com.example.database.datasource.DiskDataSource;
import com.example.database.entity.output.Day;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 *
 */
public class WeatherDataStoreImpl implements WeatherDataStore {

    private final DiskDataSource diskDataSource;
    private final WeatherConnection weatherConnection;
    private final DailyWeatherMapper weatherMapper;

    @Inject
    public WeatherDataStoreImpl(DiskDataSource diskDataSource, WeatherConnection weatherConnection,
                                DailyWeatherMapper weatherMapper){
        if(diskDataSource==null || weatherConnection == null || weatherMapper == null)
            throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
        this.diskDataSource = diskDataSource;
        this.weatherConnection = weatherConnection;
        this.weatherMapper = weatherMapper;
    }

    @Override
    public Observable<CurrentWeather> getCurrentDayWeather(int cityId) {
        return weatherConnection.currentWeather(String.valueOf(cityId));
    }

    @Override
    public Observable<CurrentWeather> getCurrentDayWeather(double lon, double lat) {
        return weatherConnection.currentWeather(String.valueOf(lat), String.valueOf(lon));
    }

    @Override
    public Observable<List<DailyWeather>> getWeekForecast(int cityId) {
        return weatherConnection.forecastByCityId(String.valueOf(cityId))
                .flatMap(Observable::from)
                .flatMap(this::combineDaily)
                .toList();

    }

    private Observable<DailyWeather> combineDaily(Day day){
        return Observable.combineLatest(Observable.just(day),
                Observable.just(diskDataSource.getCityBy(day.getCityId())),
                Observable.just(diskDataSource.getConditionBy(day.getConditionId())),
                weatherMapper::transform);
    }

    @Override
    public Observable<List<DailyWeather>> getWeekForecast(double lon, double lat) {
        return weatherConnection.forecastByCoords(String.valueOf(lat), String.valueOf(lon))
                .flatMap(Observable::from)
                .flatMap(this::combineDaily)
                .toList();
    }

    @Override
    public Observable<List<DailyWeather>> getHistoryForecast(int offset, int count) {
        return Observable.just(diskDataSource.getDaysBy(offset, count, true))
                .flatMap(Observable::from)
                .flatMap(this::combineDaily)
                .toList();
    }
}
