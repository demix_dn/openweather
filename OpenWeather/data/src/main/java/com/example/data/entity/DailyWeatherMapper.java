package com.example.data.entity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.data.entity.daily.DailyItemDTO;
import com.example.data.entity.daily.DailyResponseDTO;
import com.example.data.entity.daily.DailyWeather;
import com.example.database.entity.output.City;
import com.example.database.entity.output.Condition;
import com.example.database.entity.output.Day;
import com.example.database.entity.DayRO;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Date: 08.09.2016
 * Time: 15:47
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Singleton
public class DailyWeatherMapper {

    @Inject
    public DailyWeatherMapper() {
        //empty
    }

    public Observable<List<DayRO>> transformRx(@NonNull DailyResponseDTO dailyResponse){
        return Observable.just(transform(dailyResponse));
    }

    @Nullable
    public List<DayRO> transform(@NonNull DailyResponseDTO dailyResponse){
        int cityId = dailyResponse.getCity()==null ? 0 : dailyResponse.getCity().getId();
        List<DayRO> daysList = new ArrayList<>();
        if(dailyResponse.getDailyItems() == null){
            return null;
        }
        for(DailyItemDTO dailyItem : dailyResponse.getDailyItems()){
            DayRO dayRO = transform(cityId, dailyItem);
            daysList.add(dayRO);
        }
        return daysList;
    }

    @NonNull
    private DayRO transform(int cityId, DailyItemDTO dailyItem) {
        DayRO dayRO = new DayRO();
        dayRO.setCityId(cityId);
        dayRO.setDate(dailyItem.getDt());
        dayRO.setTempDay(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getDay());
        dayRO.setTempMin(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getMin());
        dayRO.setTempMax(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getMax());
        dayRO.setTempNight(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getNight());
        dayRO.setTempEve(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getEve());
        dayRO.setTempMorn(dailyItem.getTemp()==null ? 0d : dailyItem.getTemp().getMorn());
        dayRO.setPressure(dailyItem.getPressure());
        dayRO.setHumidity(dailyItem.getHumidity());
        if(dailyItem.getConditions()!=null && dailyItem.getConditions().get(0)!=null)
            dayRO.setConditionId(dailyItem.getConditions().get(0).getId());
        dayRO.setWindSpeed(dailyItem.getSpeed());
        dayRO.setWindDeg(dailyItem.getDeg());
        dayRO.setClouds(dailyItem.getClouds());
        return dayRO;
    }

    @NonNull
    public DailyWeather transform(@NonNull Day day, @NonNull City city, @NonNull Condition condition){
        return DailyWeather.builder()
                .date(day.getDate())
                .tempMin(day.getTempMin())
                .tempDay(day.getTempDay())
                .tempMax(day.getTempMax())
                .tempNight(day.getTempNight())
                .tempEve(day.getTempEve())
                .tempMorn(day.getTempMorn())
                .pressure(day.getPressure())
                .humidity(day.getHumidity())
                .conditionId(condition.getId())
                .titleEn(condition.getTitleEn())
                .descriptionEn(condition.getDescriptionEn())
                .titleRu(condition.getTitleRu())
                .descriptionRu(condition.getDescriptionRu())
                .iconDay(condition.getIconDay())
                .iconNight(condition.getIconNight())
                .windSpeed(day.getWindSpeed())
                .windDeg(day.getWindDeg())
                .clouds(day.getClouds())
                .cityId(city.getId())
                .cityName(city.getName())
                .build();
    }
}
