package com.example.data.entity.current;

import com.example.data.entity.daily.CoordDTO;
import com.example.data.entity.daily.ConditionDTO;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 14.09.2016
 * Time: 15:56
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class CurrentResponseDTO {

    @SerializedName("coord")
    @Expose
    CoordDTO coord;

    @SerializedName("weather")
    @Expose
    ConditionDTO condition;

    @SerializedName("base")
    @Expose
    String base;

    @SerializedName("main")
    @Expose
    MainDTO main;

    @SerializedName("wind")
    @Expose
    private WindDTO wind;

    @SerializedName("clouds")
    @Expose
    private CloudsDTO clouds;

    @SerializedName("dt")
    @Expose
    private int dt;

    @SerializedName("sys")
    @Expose
    SysDTO sys;

    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("cod")
    @Expose
    int code;
}
