package com.example.data.entity.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Date: 08.09.2016
 * Time: 15:51
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
public class DailyItemDTO {
    @SerializedName("dt")
    @Expose
    private int dt;

    @SerializedName("temp")
    @Expose
    private DailyTempDTO temp;

    @SerializedName("pressure")
    @Expose
    private double pressure;

    @SerializedName("humidity")
    @Expose
    private int humidity;

    @SerializedName("speed")
    @Expose
    private double speed;

    @SerializedName("deg")
    @Expose
    private int deg;

    @SerializedName("clouds")
    @Expose
    private int clouds;

    @SerializedName("weather")
    @Expose
    private List<ConditionDTO> conditions = new ArrayList<>();

}
