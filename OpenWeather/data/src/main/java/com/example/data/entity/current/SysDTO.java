package com.example.data.entity.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 14.09.2016
 * Time: 16:11
 *
 * @author Aleks Sander
 *         Project TestDagger
 */
@Data
@Builder
public class SysDTO {
    @SerializedName("message")
    @Expose
    double message;
    @SerializedName("country")
    @Expose
    String country;
    @SerializedName("sunrise")
    @Expose
    int sunrise;
    @SerializedName("sunset")
    @Expose
    int sunset;

    public static SysDTO getDefault() {
        return SysDTO.builder().country(null).message(0).sunrise(0).sunset(0).build();
    }
}
