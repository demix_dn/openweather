
package com.example.data.entity.daily;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class CityDTO {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("coord")
    @Expose
    private CoordDTO coord;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("population")
    @Expose
    private int population;
}
