package com.example.data;

import com.example.data.entity.current.CurrentWeather;
import com.example.database.entity.output.Day;

import java.util.List;

import rx.Observable;

public interface WeatherConnection {

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link Day}.
     * Without schedulers.
     *
     * @param lat latitude of device
     * @param lon longitude of device
     */
    @SuppressWarnings("JavadocReference")
    Observable<List<Day>> forecastByCoords(String lat, String lon);

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link Day}.
     * Without schedulers.
     *
     * @param id id of city
     */
    @SuppressWarnings("JavadocReference")
    Observable<List<Day>> forecastByCityId(String cityId);

    /**
     * Get an {@link rx.Observable} which will emit a {@link CurrentWeather}.
     * Without schedulers.
     *
     * @param lat latitude of device
     * @param lon longitude of device
     */
    @SuppressWarnings("JavadocReference")
    Observable<CurrentWeather> currentWeather(String lat, String lon);

    /**
     * Get an {@link rx.Observable} which will emit a {@link CurrentWeather}.
     * Without schedulers.
     *
     * @param id id of city
     */
    @SuppressWarnings("JavadocReference")
    Observable<CurrentWeather> currentWeather(String cityId);
}
