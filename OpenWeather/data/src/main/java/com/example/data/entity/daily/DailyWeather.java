package com.example.data.entity.daily;

import lombok.Builder;
import lombok.Data;

/**
 * Date: 15.09.2016
 * Time: 16:45
 *
 * @author Aleks Sander
 *         Project OpenWeather
 */
@Data
@Builder
public class DailyWeather {

    private int date;
    private double tempDay;
    private double tempMin;
    private double tempMax;
    private double tempNight;
    private double tempEve;
    private double tempMorn;
    private double pressure;
    private int humidity;
    private int conditionId;
    private String titleEn;
    private String descriptionEn;
    private String titleRu;
    private String descriptionRu;
    private String iconDay;
    private String iconNight;
    private double windSpeed;
    private double windDeg;
    private int clouds;
    private int cityId;
    private String cityName;
}
