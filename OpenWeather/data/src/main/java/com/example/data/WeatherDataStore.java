package com.example.data;

import com.example.data.entity.current.CurrentWeather;
import com.example.data.entity.daily.DailyWeather;

import java.util.List;

import rx.Observable;

/**
 * Created by Aleksandr on 22.07.2016 in Weather.
 * Interface that represents a data store from where data is retrieved.
 */
public interface WeatherDataStore {

    /**
     * Get an {@link rx.Observable} which will emit item of {@link CurrentWeather}.
     *
     * @param cityId The id to retrieve weather data.
     */
    Observable<CurrentWeather> getCurrentDayWeather(int cityId);

    /**
     * Get an {@link rx.Observable} which will emit item of {@link CurrentWeather}.
     *
     * @param lon The longitude to retrieve weather data.
     * @param lat The latitude to retrieve weather data.
     */
    Observable<CurrentWeather> getCurrentDayWeather(double lon, double lat);

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link DailyWeather}.
     *
     * @param cityId The id to retrieve weather data.
     */
    Observable<List<DailyWeather>> getWeekForecast(int cityId);

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link DailyWeather}.
     *
     * @param lon The longitude to retrieve weather data.
     * @param lat The latitude to retrieve weather data.
     */
    Observable<List<DailyWeather>> getWeekForecast(double lon, double lat);

    /**
     * Get an {@link rx.Observable} which will emit a List of {@link DailyWeather}.
     *
     * @param offset The id to retrieve weather data.
     * @param count The count to retrieve of weather items
     *
     */
    Observable<List<DailyWeather>> getHistoryForecast(int offset, int count);
}
