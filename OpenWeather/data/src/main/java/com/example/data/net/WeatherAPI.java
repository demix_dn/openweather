package com.example.data.net;

import com.example.data.entity.current.CurrentResponseDTO;
import com.example.data.entity.daily.DailyResponseDTO;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * The {@link WeatherAPI} class provides access for weather api.
 *
 *
 * @author Aleks Sander
 * @since 1.0
 */
public interface WeatherAPI {

    /**
     * Forecasts include daily weather by geographic coordinates.
     * API responds with exact result. All weather data can be obtained in JSON
     * @param lat latitude of device
     * @param lon longitude of device
     * @param cnt day count (1..16)
     * @param appId always need use the value {@link ApiConst.PARAMS.APPID}
     * @param units always need use the value {@link ApiConst.PARAMS.UNIT_METRICS}
     * @return all server data in wrapper {@link DailyResponseDTO}
     */
    @SuppressWarnings("JavadocReference")
    @GET("data/2.5/forecast/daily")
    Observable<DailyResponseDTO> dailyForecastBy(@Query("lat") String lat,
                                                 @Query("lon") String lon,
                                                 @Query("cnt") String cnt,
                                                 @Query("appid") String appId,
                                                 @Query("units") String units);
    /**
     * Forecasts include daily weather by city id.
     * API responds with exact result. All weather data can be obtained in JSON
     * @param cityId id of city
     * @param cnt day count (1..16)
     * @param appId always need use the value {@link ApiConst.PARAMS.APPID}
     * @param units always need use the value {@link ApiConst.PARAMS.UNIT_METRICS}
     * @return all server data in wrapper {@link DailyResponseDTO}
     */
    @SuppressWarnings("JavadocReference")
    @GET("data/2.5/forecast/daily")
    Observable<DailyResponseDTO> dailyForecastBy(@Query("id") String cityId,
                                                 @Query("cnt") String cnt,
                                                 @Query("appid") String appId,
                                                 @Query("units") String units);

    /**
     * Access current weather data by geographic coordinates.
     * API responds with exact result.
     *
     * @param lat latitude of device
     * @param lon longitude of device
     * @param appId always need use the value {@link ApiConst.PARAMS.APPID}
     * @param units always need use the value {@link ApiConst.PARAMS.UNIT_METRICS}
     * @return all server data in wrapper {@link CurrentResponseDTO}
     */
    @SuppressWarnings("JavadocReference")
    @GET("data/2.5/weather")
    Observable<CurrentResponseDTO> currentWeather(@Query("lat") String lat,
                                                  @Query("lon") String lon,
                                                  @Query("appid") String appId,
                                                  @Query("units") String units);

    /**
     * Access current weather data by city id.
     * API responds with exact result.
     *
     * @param cityId id of city
     * @param appId always need use the value {@link ApiConst.PARAMS.APPID}
     * @param units always need use the value {@link ApiConst.PARAMS.UNIT_METRICS}
     * @return all server data in wrapper {@link CurrentResponseDTO}
     */
    @SuppressWarnings("JavadocReference")
    @GET("data/2.5/weather")
    Observable<CurrentResponseDTO> currentWeather(@Query("id") String cityId,
                                                  @Query("appid") String appId,
                                                  @Query("units") String units);
}
