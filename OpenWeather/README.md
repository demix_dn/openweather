#Open Weather
Project with weather using forecast API from [Open Weather Map](http://openweathermap.org/) .

Using [Dagger 2](https://github.com/google/dagger), [Retrofit 2](http://square.github.io/retrofit/),  [RxJava](https://github.com/ReactiveX/RxJava), [Realm](https://realm.io/docs/java/latest/), [MVP pattern and application layers](https://github.com/android10/Android-CleanArchitecture).

**Education project**. 